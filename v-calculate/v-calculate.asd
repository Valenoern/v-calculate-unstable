(defsystem "v-calculate"
	:author "Valenoern"
	:description "calculator package for v-refracta"
	:licence "AGPL 3 or later"
		;; because this thing unfortunately has similarities to a number of web apps and npm modules
	:version "0.2.2"
	
	:depends-on (
		"uiop"  ; for saving cells to disk
		)
	:components (
		(:file "sheet")  ; :v-refracta.calculate.sheet
		(:file "clicker" ; :v-refracta.calculate.clicker
			:depends-on ("sheet"))
))
